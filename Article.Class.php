<?php


class Article extends Publication {

    protected $author;


    public function getAuthor(){
        return $this->author;
    }


   //Constructor
    public function __construct($id, $meta_description, $meta_keywords, $meta_title, $title, $type, $intro_text, $full_text, $author){
        parent::__construct($id, $meta_description, $meta_keywords, $meta_title, $title, $type, $intro_text, $full_text);

        $this->author = $author;
    }


    public function getShortPreview(){
        $str = '<h4>' . $this->getTitle() . '</h4>';
        $str .= '<p>' . $this -> getIntroText(). '</p>';
        $str .= '<b>Author: ' . $this->getAuthor() . '</b><br>';
        $str .= '<b><a href="publication.php?id=' . $this->getId() . '" >READ MORE</a></b>';
        $str .= '<hr>';
        return $str;
    }


}