<?php

try{
	$pdo = new PDO('mysql:host=localhost;dbname=info_portal','root','');
	$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	$pdo->exec("SET NAMES 'utf8'");
	
}catch(PDOException $e){
	echo "Не получилось подключиться к Базе Данных info_portal.<br>";
	echo $e->getMessage();
	exit();
}