<?php

require_once 'db_connect.php';
require_once 'Publication.Class.php';
require_once 'News.Class.php';
require_once 'Article.Class.php';


class PublicationsWriter {

    public $publications = array();
    public function __construct($type, PDO $pdo){
        $query = "SELECT * FROM library WHERE type =:type";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':type', $type);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        if(empty($rows)) {
            return null;
        }
        foreach ($rows as $row) {
            if ($row['type'] == 'article') {
                $this->publications[] = new Article(
                    $row['id'],
                    $row['meta_description'],
                    $row['meta_keywords'],
                    $row['meta_title'],
                    $row['title'],
                    $row['type'],
                    $row['intro_text'],
                    $row['full_text'],
                    $row['author']
                );
            } else if ($row['type'] == 'new') {
                $this->publications[] = new News(
                    $row['id'],
                    $row['meta_description'],
                    $row['meta_keywords'],
                    $row['meta_title'],
                    $row['title'],
                    $row['type'],
                    $row['intro_text'],
                    $row['full_text'],
                    $row['source']
                );
            }
        }
    }
}
