<?php


abstract class Publication{

    protected $id;
    protected $meta_description;
    protected $meta_keywords;
    protected $meta_title;
    protected $title;
    protected $type;
    protected $intro_text;
    protected $full_text;


    public function getId(){
        return $this->id;
    }
    public function getMetaDescription(){
        return $this->meta_description;
    }
    public function getMetaKeywords(){
        return $this->meta_keywords;
    }
    public function getMetaTitle(){
        return $this->meta_title;
    }
    public function getType(){
        return $this->type;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getIntroText(){
        return $this->intro_text;
    }
    public function getFullText(){
        return $this->full_text;
    }


    // Constructor
    public function __construct($id, $meta_description, $meta_keywords, $meta_title, $title, $type, $intro_text, $full_text){
        $this->id = $id;
        $this->meta_description = $meta_description;
        $this->meta_keywords = $meta_keywords;
        $this->meta_title = $meta_title;
        $this->type = $type;
        $this->title = $title;
        $this->intro_text = $intro_text;
        $this->full_text = $full_text;

    }


    public function setID($id){
        $this->id = $id;
    }


    //Static factory
    public static function create($id, PDO $pdo){
        $query = "SELECT * FROM library WHERE id=:id";
        $stmt = $pdo->prepare($query);
        $stmt -> bindValue(':id', $id);
        $stmt -> execute();
        $row = $stmt->fetchObject();
        if (empty($row)) {
            return null;
        }
        $publication = '';
        if($row->type == 'article'){
            $publication = new Article(
                $row->id,
                $row->meta_description,
                $row->meta_keywords,
                $row->meta_title,
                $row->title,
                $row->type,
                $row->intro_text,
                $row->full_text,
                $row->author
            );
        }else if($row->type == 'new'){
            $publication = new News(
                $row->id,
                $row->meta_description,
                $row->meta_keywords,
                $row->meta_title,
                $row->title,
                $row->type,
                $row->intro_text,
                $row->full_text,
                $row->source
            );
        }
        $publication->setID($row->id);
        return $publication;
    }
}