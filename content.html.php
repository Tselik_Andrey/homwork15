<?php require_once 'header.index.php'; ?>

<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2> Articles</h2>
                <?php
                foreach ($articles->publications as $publication) {
                    echo $publication -> getShortPreview();
                };
                ?>
            </div>

            <div class="col-md-6">
                <h2> News</h2>
                <?php
                foreach ($news->publications as $publication) {
                    echo $publication -> getShortPreview();
                };
                ?>
            </div>
        </div>
    </div>
</section>


