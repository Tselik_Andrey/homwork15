<?php require_once 'header.publication.php'; ?>

<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2><?= $publication->getTitle();?></h2>
                <p><?= $publication->getFullText(); ?></p>

                <?php  if($publication->getType() == 'new'): ?>
                <h6>Source: <?= $publication->getSource(); ?></h6>
                <?php else: ?>
                <h6>Author: <?= $publication->getAuthor(); ?></h6>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>