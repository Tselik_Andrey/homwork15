<?php

require_once 'db_connect.php';
require_once 'Publication.Class.php';
require_once 'Article.Class.php';
require_once 'News.Class.php';


if(isset($_GET['id'])){
    $id = $_GET['id'];
}

$publication = Publication::create($id, $pdo);


$title = $publication->getMetaTitle();
$description = $publication->getMetaDescription();
$keywords = $publication->getMetaKeywords();


include 'publication.html.php';
