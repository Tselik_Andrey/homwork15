<?php

class News extends Publication {

    protected $source;


    public function getSource(){
        return $this->source;
    }


    public function __construct($id, $meta_description, $meta_keywords, $meta_title, $title, $type, $intro_text, $full_text, $source){
        parent::__construct($id, $meta_description, $meta_keywords, $meta_title, $title, $type, $intro_text, $full_text);

        $this->source = $source;
    }


    public function getShortPreview(){
        $str = '<h4>' . $this->getTitle() . '</h4>';
        $str .= '<p>' . $this -> getIntroText(). '</p>';
        $str .= '<b>Source: ' . $this->getSource() . '</b><br>';
        $str .= '<b><a href="publication.php?id=' . $this->getId() . '" >READ MORE</a></b>';
        $str .= '<hr>';
        return $str;
    }
}